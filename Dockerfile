FROM nginx:1.25.3
EXPOSE 80
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY index.html /usr/share/nginx/html/